# Your Daily Friend
A Java Springboot application that sends daily positive quotes notifications to your device. Everyone who subscribes will start their day happily.<br/>
<br/>
You can acces our application https://yourdailyfriends.herokuapp.com<br/>
Quotes were taken from https://type.fit/api/quotes

## References
[Link 1](https://golb.hplar.ch/2018/01/Sending-Web-push-messages-from-Spring-Boot-to-Browsers.html)<br/>
[Link 2](https://golb.hplar.ch/2019/08/webpush-java.html)<br/>

## License
(c) Icon by [Icons8](https://icons8.com/)