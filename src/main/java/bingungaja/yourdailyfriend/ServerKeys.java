package bingungaja.yourdailyfriend;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class ServerKeys {

    private final AppProperties appProperties;
    private final CryptoService cryptoService;
    private ECPublicKey publicKey;
    private ECPrivateKey privateKey;
    private byte[] publicKeyUncompressed;
    private String publicKeyBase64;

    public ServerKeys(AppProperties appProperties, CryptoService cryptoService) {
        this.appProperties = appProperties;
        this.cryptoService = cryptoService;
    }

    public ECPublicKey getPublicKey() {
        return this.publicKey;
    }

    public ECPrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public byte[] getPublicKeyUncompressed() {
        return this.publicKeyUncompressed;
    }

    public String getPublicKeyBase64() {
        return this.publicKeyBase64;
    }

    @PostConstruct
    private void initKeys() {
        Path appServerPublicKeyFile = Paths.get(this.appProperties.getServerPublicKeyPath());
        Path appServerPrivateKeyFile = Paths.get(this.appProperties.getServerPrivateKeyPath());

        if (Files.exists(appServerPublicKeyFile) && Files.exists(appServerPrivateKeyFile)) {
            // Loads the key pair
            try {
                byte[] appServerPublicKey = Files.readAllBytes(appServerPublicKeyFile);
                byte[] appServerPrivateKey = Files.readAllBytes(appServerPrivateKeyFile);

                this.publicKey = (ECPublicKey) this.cryptoService.convertX509ToECPublicKey(appServerPublicKey);
                this.privateKey = (ECPrivateKey) this.cryptoService.convertPKCS8ToECPrivateKey(appServerPrivateKey);
            }
            catch (IOException | InvalidKeySpecException e) {
                YourDailyFriendApplication.logger.error("read files", e);
            }
        }
        else {
            // Creates the key pair and stores it in the root of the project into two files
            try {
                KeyPair pair = this.cryptoService.getKeyPairGenerator().generateKeyPair();
                this.publicKey = (ECPublicKey) pair.getPublic();
                this.privateKey = (ECPrivateKey) pair.getPrivate();
                
                Files.write(appServerPublicKeyFile, this.publicKey.getEncoded());
                Files.write(appServerPrivateKeyFile, this.privateKey.getEncoded());
            }
            catch (IOException e) {
                YourDailyFriendApplication.logger.error("write files", e);
            }
        }

        this.publicKeyUncompressed = CryptoService.toUncompressedECPublicKey(this.publicKey);
        this.publicKeyBase64 = Base64.getUrlEncoder().withoutPadding().encodeToString(this.publicKeyUncompressed);
    }

}