package bingungaja.yourdailyfriend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@EnableScheduling
public class YourDailyFriendApplication {

	public final static Logger logger = LoggerFactory.getLogger(YourDailyFriendApplication.class);

	/**
	 * Main program
	 */
	public static void main(String[] args) {
		SpringApplication.run(YourDailyFriendApplication.class, args);
	}

	@Bean
	/**
	 * A bean that instantiate a WebClient
	 * @return webClient : a Web Client object
	 */
	public WebClient webClient() {
		return WebClient.create();
	}

}
