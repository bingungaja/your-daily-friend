package bingungaja.yourdailyfriend;

import bingungaja.yourdailyfriend.models.QuotesDB;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.client.WebClient;

@Controller
public class MainController {
	
    @GetMapping("/")
    public String main() {
        return "index.html";
    }
    
    @GetMapping("/index.html")
    public String main2() {
        return "index.html";
    }
}
