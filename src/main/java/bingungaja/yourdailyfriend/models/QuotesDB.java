package bingungaja.yourdailyfriend.models;

import java.util.ArrayList;
import java.util.Random;

public class QuotesDB {
	
	private static ArrayList<QuotesDB> quotes = new ArrayList<QuotesDB>();
	private String text;
	private String author;
	
	/**
	 * Setter for the quotes text
	 * @param text : the quotes
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Getter for the quotes text
	 * @return this.text : this QuotesDB object text
	 */
	public String getText() {
		return this.text;
	}
	
	/**
	 * Setter for the quotes author
	 * @param author : the author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * Getter for the quotes author
	 * @return this.author : the author
	 */
	public String getAuthor() {
		return this.author;
	}
	
	/**
	 * Add the quote to quotes list
	 * @param quote : the quote to be added quotes list
	 */
	public static void addQuotes(QuotesDB quote) {
		QuotesDB.quotes.add(quote);
	}
	
	/**
	 * this method return a QuotesDB object that have its own quote
	 * @return a QuotesDB object
	 */
	public static QuotesDB getQuotes() {
		Random rd = new Random();
		if (QuotesDB.quotes.size() == 0) {
			return null;
		}
		int next = rd.nextInt(QuotesDB.quotes.size());
		return QuotesDB.quotes.get(next);
	}
	
	@Override
	/**
	 * String representation of the object
	 * @return a String representation of the Object
	 */
	public String toString() {
		return String.format("\"%s\" - %s", this.getText(), this.getAuthor());
	}
	
}
