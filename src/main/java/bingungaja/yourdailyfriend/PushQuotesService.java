package bingungaja.yourdailyfriend;

import bingungaja.yourdailyfriend.models.QuotesDB;

import org.springframework.web.reactive.function.client.WebClient;

public class PushQuotesService {
	
	private final WebClient webClient;
	private final String apiList[] = {"https://type.fit/api/quotes"};
	
	/**
	 * Constructor of the service
	 * @param webClient : we will use it to make a get request to API(s)
	 * @param fcmClient : we will use it to send the quotes to device(s)
	 */
	public PushQuotesService(WebClient webClient) { 
		this.webClient = webClient;
	}
	
	public QuotesDB parsingQuotesFromAPI() {
		QuotesDB quotes = new QuotesDB();
		for (String api : apiList) {
			String apiNow = this.webClient.get().uri(api).retrieve().bodyToMono(String.class).block();
			quotes = parsingQuotes(apiNow);
		}
		return quotes;
	}
	
	/**
	 * Method to parse the quotes from an API
	 * Each quotes in the API should be in the following format:
	 * 		{"text":"<the_text>","author":"<the_author>"}
	 * 		Example: {"text":"We have committed the Golden Rule to memory; let us now commit it to life.","author":"Edwin Markham"}
	 * Please note that the documentation could be changed depending on the API that we use
	 * @param quotesList : the resulted String from get request to the API
	 * @return QuotesDB object : a QuotesDB object
	 */
	public QuotesDB parsingQuotes(String quotesList) {
		QuotesDB quotes = new QuotesDB();
		int len = quotesList.length();
		int i = 0;

		while (i >= 0) {
			String text = "";
			String author = "";
			
			i = quotesList.indexOf("\"text\"",i);
			int j = i;
			if (i < 0) {
				break;
			}
			i = quotesList.indexOf("\"author\"",i);
			if (i < 0) {
				break;
			}
			
			j += 7;
			while(j < i) {
				text += quotesList.charAt(j);
				j++;
			}
			
			i += 9;
			while(i < len && quotesList.charAt(i) != '}') {
				author += quotesList.charAt(i);
				i++;
			}
			text = text.trim();
			author = author.trim();
			
			text = text.substring(1, text.length() - 2);
			if (!author.equals("null")) {
				author = author.substring(1, author.length() - 1);
			}
			
			QuotesDB newQuote = new QuotesDB();
			newQuote.setText(text);
			newQuote.setAuthor(author);
			quotes.addQuotes(newQuote);
		}
		return quotes;
	}
	
}
