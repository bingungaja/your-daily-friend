if ("serviceWorker" in navigator) {
  try {
    checkSubscription();
    init();
  } catch (e) {
    console.error('error init(): ' + e);
  }

  $("#subscribeButton").click(function() {
	  subscribe().catch(e => {
		  if (Notification.permission === 'denied') {
        console.warn('Permission for notifications was denied');
      } else {
        console.error('error subscribe(): ' + e);
      }   
	  });
  });

  $("#unsubscribeButton").click(function() {
    unsubscribe().catch(e => console.error('error unsubscribe(): ' + e)); 
  });
}

async function checkSubscription() {
  const registration = await navigator.serviceWorker.ready;
  const subscription = await registration.pushManager.getSubscription();
  if (subscription) {
    const response = await fetch("/isSubscribed", {
      method: 'POST',
      body: JSON.stringify({endpoint: subscription.endpoint}),
      headers: {
        "content-type": "application/json"
      }
    });

    const subscribed = await response.json();
    if (subscribed) {
      $("#subscribeButton").attr("disabled", true);
      $("#unsubscribeButton").removeAttr("disabled");
    }

    return subscribed;
  }
  
  return false;
}

async function init() {
  fetch('/publicSigningKey')
    .then(response => response.arrayBuffer())
    .then(key => this.publicSigningKey = key)
    .finally(() => console.info('Application Server Public Key fetched from the server'));

  await navigator.serviceWorker.register("/serviceWorker.js", {
    scope: "/"
  });

  await navigator.serviceWorker.ready;
  console.info('Service Worker has been installed and is ready');
  navigator.serviceWorker.addEventListener('message', event => displayLastMessages());

  displayLastMessages();
}

function displayLastMessages() {
  caches.open('data').then(dataCache => {
    dataCache.match('quote')
    .then(response => response ? response.text() : '')
    .then(txt => $("#quote").text(txt).each(setBodyScale()));
  });
  console.log("bar");
}

setInterval(function() {
  console.log("foo");
  displayLastMessages();
}, 5000);

async function unsubscribe() {
  const registration = await navigator.serviceWorker.ready;
  const subscription = await registration.pushManager.getSubscription();
  if (subscription) {
    const successful = await subscription.unsubscribe();
    if (successful) {
      console.info('Unsubscription successful');

      await fetch("/unsubscribe", {
        method: 'POST',
        body: JSON.stringify({endpoint: subscription.endpoint}),
        headers: {
          "content-type": "application/json"
        }
      });
      console.info('Unsubscription info sent to the server');

      $("#subscribeButton").removeAttr("disabled");
      $("#unsubscribeButton").attr("disabled", true);
    }
    else {
      console.error('Unsubscription failed');
    }
  }
}

async function subscribe() {
  const registration = await navigator.serviceWorker.ready;
  const subscription = await registration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: this.publicSigningKey
  });
  console.info(`Subscribed to Push Service: ${subscription.endpoint}`);

  await fetch("/subscribe", {
    method: 'POST',
    body: JSON.stringify(subscription),
    headers: {
      "content-type": "application/json"
    }
  });
  console.info('Subscription info sent to the server');

  $("#subscribeButton").attr("disabled", true);
  $("#unsubscribeButton").removeAttr("disabled");
}

setInterval(async function(){
  const response = await fetch('todaysQuote');
  const newQuote = await response.text();
  const dataCache = await caches.open('data');
  await dataCache.put('quote', new Response(newQuote));
  console.log("update the quote");
}, 5000);
